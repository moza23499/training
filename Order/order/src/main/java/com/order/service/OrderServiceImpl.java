package com.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.order.entities.Order;
import com.order.exceptions.OrderNotFoundException;
import com.order.repo.OrderRepo;
@Service
@Transactional
public class OrderServiceImpl implements OrderService{

	@Autowired
	private OrderRepo orderRepo;
	
	@Override
	public List<Order> getAll() {
		return orderRepo.findAll();
	}

	public Order getById(int id) {
		Order order= orderRepo.findById((int) id)
				.orElseThrow(()-> new OrderNotFoundException("order with id "+ id +" is not found"));
		return order;
	}
	@Override
	public Order addOrder(Order order) {
		Order savedOrder=orderRepo.save(order);
		return savedOrder;
	}

	@Override
	public Order updateOrder(Order order) {
		Order updatedOrder=orderRepo.save(order);
		return updatedOrder;
	}

}
