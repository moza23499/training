package com.inventory.service;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inventory.entities.Inventory;
import com.inventory.exceptions.InventoryNotFoundException;
import com.inventory.repo.InventoryRepo;


@Service
@Transactional
public class InventoryServiceImpl implements InventoryService{

	@Autowired
	private InventoryRepo inventoryRepo;

	@Override
	public List<Inventory> getAll() {
		
		return inventoryRepo.findAll();
	}

	@Override
	public Inventory addInventory(Inventory inventory) {
		inventoryRepo.save(inventory);
		return inventory;
	}
	
	
	
	
	@Override
	public Inventory updateInventory(int inventoryId, Inventory inventory) {
		Inventory inventoryToUpdate=getById(inventoryId);
		
		inventoryToUpdate.setTotalProdCount(inventory.getTotalProdCount());
		return inventoryToUpdate;
	
	}



	@Override
	public Inventory getById(int inventoryId) {
		Inventory inventory = inventoryRepo.findById(inventoryId)
				.orElseThrow(()-> new InventoryNotFoundException("inventory with id "+ inventoryId +" is not found"));
		return inventory;
	}





	

	

	



	
	
	

}
