package com.prodstore.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecureRestController {

	@GetMapping(path="home")
	public String home(){
		return "hello to home";
	}
	
	@GetMapping(path="admin")
	public String homeAdmin(){
		return "hello to admin";
	}
	
	@GetMapping(path="agent")
	public String homeMgr(){
		return "hello to agent";
	}
	
	@GetMapping(path="viewer")
	public String homeEmp(){
		return "hello to viewer";
	}
	
}