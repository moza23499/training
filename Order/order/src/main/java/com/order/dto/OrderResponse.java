package com.order.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//@Getter
//@Setter
//@NoArgsConstructor
//@Data
//@AllArgsConstructor
public class OrderResponse {
	private int orderId;
	private String status;
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public OrderResponse(int orderId, String status) {
		super();
		this.orderId = orderId;
		this.status = status;
	}
	public OrderResponse() {
		
	}
	
	
	
	
}
