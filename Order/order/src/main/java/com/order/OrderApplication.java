package com.order;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.order.service.OrderService;

import brave.sampler.Sampler;

@EnableEurekaClient
@SpringBootApplication
@EnableHystrix
public class OrderApplication implements CommandLineRunner{
	

	
	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}
	@Autowired
	private OrderService orderService;
	


	public static void main(String[] args) {
		SpringApplication.run(OrderApplication.class, args);
	}
	@LoadBalanced
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	

	@Override
	public void run(String... args) throws Exception {
		
		Date orderDate=new Date();
		String currentDate= orderDate.toString();
//	
//	
//	Order order1 = new Order(1,1,1,1,1,OrderStatus.PLACED);
//	Order order2 = new Order(2,2,2,2,2,OrderStatus.SHIPPED);
//	Order order3 = new Order(3,3,3,3,3,OrderStatus.CONFIRMED);
//	
//	
//	orderService.addOrder(order1);
//	orderService.addOrder(order2);
//	orderService.addOrder(order3);
//	System.out.println("----------order details --------");
}
}
