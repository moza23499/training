package com.inventory.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.inventory.entities.Inventory;
import com.inventory.service.InventoryService;
//@EnableEurekaClient
@RestController
public class InventoryController {
	
	@Autowired
	private InventoryService inventoryService;
	//get all
	@GetMapping("inventory")
	public ResponseEntity<List<Inventory>>  getAll(){
		List<Inventory> inventory=inventoryService.getAll();		
		return ResponseEntity.ok(inventory);
	}	
	//get by id
	@GetMapping("inventory/{inventoryId}")
	public ResponseEntity<Inventory>  getAnProduct(@PathVariable(name = "inventoryId")int inventoryId){
		Inventory inventory=inventoryService.getById(inventoryId);
		return ResponseEntity.ok(inventory);
	}
	//update
	@PutMapping("inventory/{inventoryId}")
	public ResponseEntity<Inventory>  updateAnProduct
	(@PathVariable(name = "inventoryId")int inventoryId, @Valid @RequestBody Inventory inventory){
		Inventory inventoryUpdated=inventoryService.updateInventory(inventoryId, inventory);
		return ResponseEntity.status(HttpStatus.OK).body(inventoryUpdated);
	}
}








