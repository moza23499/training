package com.order.dto;

import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

//@Data
//@NoArgsConstructor
public class OrderStatusResponse {
	private int orderId;
	private String status;
	private Date date;
	public OrderStatusResponse(int orderId, String status) {
		this.orderId = orderId;
		this.status = status;
		this.date=new Date();
	}
	public OrderStatusResponse() {
		
	}
	
	
	
}
