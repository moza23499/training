package com.prodstore.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prodstore.entities.AppUser;
@Repository
public interface AppUserRepo extends JpaRepository<AppUser, Long> {
	public AppUser findByEmail(String email);
}
