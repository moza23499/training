package com.store.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.entities.Store;


@Repository
public interface StoreRepo extends JpaRepository<Store,Integer> {
	Optional<Store> findByPincode(int pincode);

}
