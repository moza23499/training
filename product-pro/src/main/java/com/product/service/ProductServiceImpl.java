package com.product.service;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.product.entities.Product;
import com.product.exceptions.ProductNotFoundException;
import com.product.repo.ProductRepo;
@Service
@Transactional
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductRepo productRepo;

	@Override
	public List<Product> getAll() {
		
		return productRepo.findAll();
	}

	@Override
	public Product addProduct(Product product) {
		productRepo.save(product);
		return product;
	}

	@Override
	public Product deleteProduct(int productId) {
		Product productToDelete=getById(productId);
		productRepo.delete(productToDelete);
		return productToDelete;
	}

	@Override
	public Product updateProduct(int productId, Product product) {
		Product productToUpdate=getById(productId);
		
		productToUpdate.setpPrice(product.getpPrice());
			return productToUpdate;
	}

	@Override
	public Product getById(int productId) {
		Product product = productRepo.findById(productId)
				.orElseThrow(()-> new ProductNotFoundException("product with id "+ productId +" is not found"));
		return product;
	}
	
	

}
