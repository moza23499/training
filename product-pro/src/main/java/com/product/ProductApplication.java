package com.product;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.product.entities.Product;
import com.product.service.ProductService;

import brave.sampler.Sampler;

@EnableEurekaClient
@SpringBootApplication
public class ProductApplication implements CommandLineRunner{
	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}

	@Autowired
	private ProductService productService;
	
	public static void main(String[] args) {
		SpringApplication.run(ProductApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	
//		
//		Product product1 = new Product("laptop", 40000.0, " microsoft surface 10th generation ");
//		Product product2 = new Product("tv", 18000.0, " oneplus y series 32 inch' ");
//		Product product3 = new Product("headphones", 5000.0, "jbl wireless headphones");
//		Product product4 = new Product("ac", 38000.0, "lloyd dual inverter split ac");
//		Product product5 = new Product("shoes", 10000.0, "gucci unisex casual shoes");
//		
//		productService.addProduct(product1);
//		productService.addProduct(product2);
//		productService.addProduct(product3);
//		productService.addProduct(product4);
//		productService.addProduct(product5);
//		
//
//		System.out.println("-------------data added----------------");
		
	}

}
