//package com.prodstore.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.prodstore.dto.AppUserRequest;
//import com.prodstore.dto.AppUserResponse;
//import com.prodstore.entities.AppUser;
//import com.prodstore.service.AppUserService;
//
//@RestController
//@RequestMapping(path ="agent" )
//public class AgentRestController {
//
//	
//	@Autowired
//	private AppUserService appUserService;
//	
//	@GetMapping(path="searchOrderForSpecificStore")
//	public AppUserResponse searchOrderForSpecificStore(@RequestBody AppUserRequest appUserRequest){
//		AppUser appUser=convertFromAppUserRequest(appUserRequest);
//		
//		appUserService.addAppUser(appUser);
//
//		
//		return new AppUserResponse(appUser.getPassword(), appUser.getEmail(), appUser.getProfile());
//	}
//
//	public AppUser convertFromAppUserRequest(AppUserRequest appUserRequest) {
//		
//		String profile= "ROLE_"+appUserRequest.getProfile().toUpperCase();
//		AppUser appUser=new AppUser
//				(appUserRequest.getName(), 
//				appUserRequest.getPassword(),
//				appUserRequest.getEmail(),
//				appUserRequest.getPhone(), 
//				appUserRequest.getAddress(), 
//				profile);
//		
//		return appUser;
//	}
//	
//	
////	@PutMapping(path="updateuser")
////	public String updateuser(){
////		
////		
////		return "hello to admin";
////	}
//	
//	@PutMapping(path="deleteuser")
//	public String deleteuser(@RequestBody AppUserRequest appUserRequest){
//		AppUser appUser=convertFromAppUserRequest(appUserRequest);
//		
//		appUserService.deleteAppUser(appUser.getEmail());
//
//		return appUser.getName();
//			
//	}
//}