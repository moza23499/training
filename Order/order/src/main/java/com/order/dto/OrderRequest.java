package com.order.dto;

import java.util.Date;

import com.order.entities.Order;
import com.order.entities.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//@Getter
//@Setter
//@NoArgsConstructor
//@Data
//@AllArgsConstructor
public class OrderRequest {
	private int agentId;
	private int customerId;
	private int productId;
	private int quantity;
	public int getAgentId() {
		return agentId;
	}
	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public OrderRequest(int agentId, int customerId, int productId, int quantity) {
		super();
		this.agentId = agentId;
		this.customerId = customerId;
		this.productId = productId;
		this.quantity = quantity;
	}
	public OrderRequest() {
	
	}
	
	
}
