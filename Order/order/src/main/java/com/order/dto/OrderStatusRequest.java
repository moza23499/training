package com.order.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

//@Data
//@NoArgsConstructor
public class OrderStatusRequest {
	private int orderId;
	private String status;
	public OrderStatusRequest() {
		
	}
	public OrderStatusRequest(int orderId, String status) {
		super();
		this.orderId = orderId;
		this.status = status;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
	
}
