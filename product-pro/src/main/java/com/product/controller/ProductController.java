package com.product.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.product.entities.Product;
import com.product.service.ProductService;


//@EnableEurekaClient
@RestController
public class ProductController {
	@Autowired
	private RestTemplate restTemplate;
	
	private Logger logger=LoggerFactory.getLogger(ProductController.class);
	@GetMapping("productservice")
	public String getProduct() {
		logger.info("inside product controller product-service");
		//String value =restTemplate.getForObject("http://localhost:8082/product-service", String.class);
		//logger.info("value return from product-service"+value);
		return "hello from product-service";
	}
	@Autowired
	private ProductService productService;
	
	
	
	
	//get all
	@GetMapping("product")
	public ResponseEntity<List<Product>>  getAll(){
		List<Product> products=productService.getAll();
		
		return ResponseEntity.ok(products);
	}
	
	//get by id
	@GetMapping("product/{productId}")
	public ResponseEntity<Product>  getAnProduct(@PathVariable(name = "productId")int productId){
		Product product=productService.getById(productId);
		return ResponseEntity.ok(product);
	}
	
	//delete
	@DeleteMapping("product/{productId}")
	public ResponseEntity<Void>  deleteAnProduct(@PathVariable(name = "productId")int productId){
		Product product=productService.deleteProduct(productId);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	
	//step 5: activate validation by apply @Valid before product in post method and
	//update method
	
	//add 
	@PostMapping("product")
	public ResponseEntity<Product>  addAnProduct(@Valid  @RequestBody Product product){
		Product productAdded=productService.addProduct(product);
		return ResponseEntity.status(HttpStatus.CREATED).body(product);
	}
	
	
	//update
	@PutMapping("product/{productId}")
	public ResponseEntity<Product>  updateAnProduct
	(@PathVariable(name = "productId")int productId, @Valid @RequestBody Product product){
		Product productUpdated=productService.updateProduct(productId, product);
		return ResponseEntity.status(HttpStatus.OK).body(productUpdated);
	}
}








