package com.inventory.service;
import java.util.*;

import com.inventory.entities.Inventory;
public interface InventoryService {
	List<Inventory> getAll();
	
	
	public Inventory updateInventory(int inventoryId, Inventory inventory);
	public Inventory getById(int inventoryId);
	public Inventory addInventory(Inventory inventory);
}
