package com.order.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
//@Getter
//@Setter
//@NoArgsConstructor
//@Data
public class Customer {
	
	private Integer customerId;
	
	private String name;
	
	private String phoneNo;
	
	private String address;

	private String email;
	
	
	
	public Integer getCustomerId() {
		return customerId;
	}



	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getPhoneNo() {
		return phoneNo;
	}



	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public Customer() {
		super();
	}



	public Customer(String name, String phoneNo, String address, String email) {
		super();
		this.name = name;
		this.phoneNo = phoneNo;
		this.address = address;
		this.email = email;
	}
	
	
	
	
}
