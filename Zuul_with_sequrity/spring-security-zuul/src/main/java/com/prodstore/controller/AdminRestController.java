package com.prodstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prodstore.dto.AppUserRequest;
import com.prodstore.dto.AppUserResponse;
import com.prodstore.entities.AppUser;
import com.prodstore.service.AppUserService;

@RestController
@RequestMapping(path ="admin" )
public class AdminRestController {
	
	@Autowired
	private AppUserService appUserService;
	
	@PostMapping(path="register")
	public AppUserResponse registernewuser(@RequestBody AppUserRequest appUserRequest){
		AppUser appUser=convertFromAppUserRequest(appUserRequest);
		
		appUserService.addAppUser(appUser);

		
		return new AppUserResponse(appUser.getPassword(), appUser.getEmail(), appUser.getProfile());
	}
	

	public AppUser convertFromAppUserRequest(AppUserRequest appUserRequest) {
		
		String profile= "ROLE_"+appUserRequest.getProfile().toUpperCase();
		AppUser appUser=new AppUser
				(appUserRequest.getName(), 
				appUserRequest.getPassword(),
				appUserRequest.getEmail(),
				appUserRequest.getPhone(), 
				appUserRequest.getAddress(), 
				profile);
		
		return appUser;
	}
	
	
	@PutMapping(path="updateuser")
	public String updateuser(){
		
		
		return "hello to admin";
	}
	
	//................................................................
	
//	@DeleteMapping(path="deleteuser")
//	public AppUserResponse deleteuser(@RequestBody AppUserRequest appUserRequest){
//		AppUser appUser=convertFromAppUserRequest(appUserRequest);
//		
//		appUserService.deleteAppUser(appUser.getEmail());
//
//		return new AppUserResponse(appUser.getPassword(), appUser.getEmail(), appUser.getProfile());
//			
//	}
//	
//	@PutMapping(path="updateuser")
//	public AppUserResponse updateuser(@RequestBody AppUserRequest appUserRequest){
//		AppUser appUser=convertFromAppUserRequest(appUserRequest);
//		
//		appUserService.updateAppUser(appUser.getId(), appUser);
//		
//		
//		return new AppUserResponse(appUser.getPassword(), appUser.getEmail(), appUser.getProfile());
//	}
//	
//	@GetMapping(path="getUserByEmail")
//	public AppUserResponse getUserByEmail(@RequestBody AppUserRequest appUserRequest){
//		AppUser appUser=convertFromAppUserRequest(appUserRequest);
//		
//		appUserService.findByEmail(appUser.getEmail());
//		return new AppUserResponse(appUser.getPassword(), appUser.getEmail(), appUser.getProfile());
//	}
	
}
